<?php

namespace Bitkorn\ShopPrepayTest\Controller;

use Bitkorn\ShopPrepay\Controller\IndexController;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    protected function setUp() : void
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            $configOverrides
        ));
        parent::setUp();
    }

    public function testConfirmationActionBasketFinalize()
    {
        $this->dispatch('/prepayconfirmation');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Bitkorn/ShopPrepay');
        $this->assertControllerName(IndexController::class);
        $this->assertControllerClass('IndexController');
        $this->assertMatchedRouteName('payment_prepay_confirmation');
    }
}
