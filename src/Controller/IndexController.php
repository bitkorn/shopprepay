<?php

namespace Bitkorn\ShopPrepay\Controller;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractShopController
{

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     *
     * @var BasketFinalizeService
     */
    protected $basketFinalizeService;

    /**
     *
     * @var PaymentNumberService
     */
    protected $paymentNumberService;

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param BasketFinalizeService $basketFinalizeService
     */
    public function setBasketFinalizeService(BasketFinalizeService $basketFinalizeService): void
    {
        $this->basketFinalizeService = $basketFinalizeService;
    }

    /**
     * @param PaymentNumberService $paymentNumberService
     */
    public function setPaymentNumberService(PaymentNumberService $paymentNumberService): void
    {
        $this->paymentNumberService = $paymentNumberService;
    }

    /**
     * Anzeige:
     * - Warenkorb
     * - Bankdaten
     * - Bestellnummer
     * - Bestaetigungslink
     * @return ViewModel|Response
     */
    public function indexAction()
    {
        if (
            !$this->basketService->existShopBasket()
            || !$this->basketService->checkBasket()
            || empty($xshopBasketEntity = $this->basketService->getXshopBasketEntity())
            || empty($xshopBasketEntity->getStorageItems())
        ) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();

        $basket = $this->basketService->getShopBasket();
        if (empty($basket[0])) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        if ($xshopBasketEntity->getBasketStatus() != 'basket') {
            /*
             * nothing payed at this time
             */
            return $this->redirect()->toRoute('shop_frontend_userbackend_userbaskets');
        }

        $viewModel->setVariable('shopBasketUnique', $this->basketService->getBasketUnique());
        $viewModel->setVariable('bankData', $this->shopService->getShopConfigurationValue('bank_data_html'));

        if (empty($basket[0]['payment_number']) || $basket[0]['payment_method'] != 'prepay') {
            /*
             * empty or foreign Payment-Number
             */
            $paymentNumber = $this->paymentNumberService->getNewPrepayNumber();
            if ($this->basketService->setPaymentNumber($paymentNumber) > 0) {
                $viewModel->setVariable('paymentNumber', $paymentNumber);
            }
        } elseif (!empty($basket[0]['payment_number'])) {
            $viewModel->setVariable('paymentNumber', $basket[0]['payment_number']);
        }

        return $viewModel;
    }

    /**
     * SET shop_basket_status='ordered'
     * $this->basketFinalizeService->basketOrdered($shopBasketUnique)
     * ...refresh the cookie!
     * @return ViewModel|Response
     */
    public function confirmationAction()
    {
        if (!$this->basketService->existShopBasket()) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $basket = $this->basketService->getShopBasket();
        if (empty($basket[0])) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        if ($basket[0]['shop_basket_status'] != 'basket') {
            return $this->redirect()->toRoute('shop_frontend_userbackend_userbaskets');
        }
        if (empty($basket[0]['payment_number'])) {
            return $this->redirect()->toRoute('shop_frontend_basket_choosepayment');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('shopBasketUnique', $this->basketService->getBasketUnique());
        $viewModel->setVariable('bankData', $this->shopService->getShopConfigurationValue('bank_data_html'));
        $viewModel->setVariable('paymentNumber', $basket[0]['payment_number']);

        $this->basketService->setPaymentMethod('prepay');
        $this->basketFinalizeService->basketOrdered($this->basketService->getBasketUnique());
        if (!empty($basket[0]['user_uuid'])) {
            $viewModel->setVariable('userEmail', $this->userService->getUserDetails($basket[0]['user_uuid'])['user_email']);
        }

        return $viewModel;
    }
}
