<?php

namespace Bitkorn\ShopPrepay\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ShopPrepayNumberTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'shop_prepay_number';

    public function saveAndGetNextShopPrepayNumber()
    {
        $insert = $this->sql->insert();
        try {
            $insert->values(['shop_prepay_number_number' => null]);
            $this->insertWith($insert);
            $contentId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_prepay_number_shop_prepay_number_id_seq');
            if (empty($contentId)) {
                return -1;
            }
            return $contentId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getLastShopPrepayNumber()
    {
        $select = $this->sql->select();
        try {
            $select->order('shop_prepay_number_id DESC')->limit(1);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateShopPrepayNumber($shopPrepayNumberId, $shopPrepayNumberNumber)
    {
        $update = $this->sql->update();
        try {
            $update->set(['shop_prepay_number_number' => $shopPrepayNumberNumber]);
            $update->where(['shop_prepay_number_id' => $shopPrepayNumberId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function saveShopPrepayNumber($shopPrepayNumber)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values(['shop_prepay_number_number' => $shopPrepayNumber]);
            $this->insertWith($insert);
            $contentId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.shop_prepay_number_shop_prepay_number_id_seq');
            if (empty($contentId)) {
                return -1;
            }
            return $contentId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteShopPrepayNumberByNumber($number)
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['shop_prepay_number_number' => $number]);
            return $this->deleteWith($delete);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
