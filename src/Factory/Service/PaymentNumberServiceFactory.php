<?php

namespace Bitkorn\ShopPrepay\Factory\Service;

use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Bitkorn\ShopPrepay\Table\ShopPrepayNumberTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PaymentNumberServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new PaymentNumberService();
		$service->setLogger($container->get('logger'));
        $service->setShopConfig($container->get('config')['bitkorn_shop']);
        $service->setShopPrepayNumberTable($container->get(ShopPrepayNumberTable::class));
		return $service;
	}
}
