<?php

namespace Bitkorn\ShopPrepay\Factory\Controller;

use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\ShopPrepay\Controller\IndexController;
use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new IndexController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopService($container->get(ShopService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setBasketFinalizeService($container->get(BasketFinalizeService::class));
        $controller->setPaymentNumberService($container->get(PaymentNumberService::class));
        return $controller;
    }
}
