<?php

namespace Bitkorn\ShopPrepay\Service;

use bitkorn\ShopPrepay\Table\ShopPrepayNumberTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Laminas\ServiceManager\ServiceLocatorInterface;

class PaymentNumberService extends AbstractService
{

    /**
     * @var array
     */
    protected $shopConfig;

    /**
     *
     * @var ShopPrepayNumberTable
     */
    protected $shopPrepayNumberTable;

    const PREPEND_CHAR = 'PREPAY_';
    const PAYMENT_NO_LENGTH = 6;

    /**
     * @param array $shopConfig
     */
    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    /**
     * @param ShopPrepayNumberTable $shopPrepayNumberTable
     */
    public function setShopPrepayNumberTable(ShopPrepayNumberTable $shopPrepayNumberTable): void
    {
        $this->shopPrepayNumberTable = $shopPrepayNumberTable;
    }

    /**
     * Get and save new shop_prepay_number_number
     *
     * @return string
     */
    public function getNewPrepayNumber()
    {
        $nextShopPrepayNumberId = $this->shopPrepayNumberTable->saveAndGetNextShopPrepayNumber();
        if ($nextShopPrepayNumberId < 0) {
            throw new \RuntimeException('shopPrepayNumberTable->saveAndGetNextShopPrepayNumber() gave < 0');
        } else {
            $newPrepayNumberNumber = self::PREPEND_CHAR . str_pad($nextShopPrepayNumberId, self::PAYMENT_NO_LENGTH, '0', STR_PAD_LEFT);
        }
        if ($this->shopPrepayNumberTable->updateShopPrepayNumber($nextShopPrepayNumberId, $newPrepayNumberNumber) > 0) {
            return $newPrepayNumberNumber;
        }
        return '';
    }
}
