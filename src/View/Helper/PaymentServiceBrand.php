<?php

namespace Bitkorn\ShopPrepay\View\Helper;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

class PaymentServiceBrand extends AbstractViewHelper
{
    const TEMPLATE = 'template/paymentServiceBrandPrepay';

	/**
	 * @return string
	 */
	public function __invoke()
	{
		$viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
		return $this->getView()->render($viewModel);
	}
}
