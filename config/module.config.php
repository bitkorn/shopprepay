<?php

namespace Bitkorn\ShopPrepay;

use Bitkorn\ShopPrepay\Controller\IndexController;
use Bitkorn\ShopPrepay\Factory\Controller\IndexControllerFactory;
use Bitkorn\ShopPrepay\Factory\Service\PaymentNumberServiceFactory;
use Bitkorn\ShopPrepay\Factory\Table\ShopPrepayNumberTableFactory;
use Bitkorn\ShopPrepay\Factory\View\Helper\PaymentServiceBrandFactory;
use Bitkorn\ShopPrepay\Service\PaymentNumberService;
use Bitkorn\ShopPrepay\Table\ShopPrepayNumberTable;
use Bitkorn\ShopPrepay\View\Helper\PaymentServiceBrand;
use Laminas\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'payment_prepay_start' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/prepay',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'payment_prepay_confirmation' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/prepayconfirmation',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'confirmation',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            // service
            PaymentNumberService::class => PaymentNumberServiceFactory::class,
            // table
            ShopPrepayNumberTable::class => ShopPrepayNumberTableFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [
            PaymentServiceBrand::class => PaymentServiceBrandFactory::class,
        ],
        'invokables' => [],
        'aliases' => [
            'paymentBrandPrepay' => PaymentServiceBrand::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'template/paymentServiceBrandPrepay' => __DIR__ . '/../view/template/paymentServiceBrand.phtml',
        ],
        'template_path_stack' => [
            'BitkornShopPrepay' => __DIR__ . '/../view',
        ],
    ],
];
