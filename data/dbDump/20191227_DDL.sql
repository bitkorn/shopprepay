create table if not exists public.shop_prepay_number
(
    shop_prepay_number_id serial not null
        constraint shop_prepay_number_pk
            primary key,
    shop_prepay_number_number varchar(100)
);

alter table public.shop_prepay_number owner to postgres;

